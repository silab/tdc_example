[![pipeline status](https://gitlab.cern.ch/silab/tdc_example/badges/master/pipeline.svg)](https://gitlab.cern.ch/silab/tdc_example/commits/master)

## [Presentation](presentation.pdf)

# TDC Example for Reusable Verification

![Block Diagram](block_diagram.png)


## Installation
 
- Install [conda](https://conda.io/miniconda.html) for Python3

- Install dependencies (check prerequisites for [cocotb](https://docs.cocotb.org/en/stable/install.html#installation-of-prerequisites) ):
```bash
conda install --yes numpy bitarray pytest pyyaml pip
pip install cocotb cocotb-test basil-daq pytest-xdist parameterized pyvsc
```

## Runing simulation

```bash
[WAVES=1] py.test [-n auto]
```

Waveforms will be in `/tmp/tdc.vcd`.

## Runing emulation

Supported hardware is [bdaq53](https://gitlab.cern.ch/silab/bdaq53/-/wikis/Hardware/Readout-Hardware#bdaq53)/[Enclustra Mercury+ KX2 (ME-KX2-160-2I-D11-P)](https://www.enclustra.com/en/products/fpga-modules/mercury-kx2/).
Requires [Vivado](https://www.xilinx.com/products/design-tools/vivado.html) to be installed.

Initlialize [SiTCP](https://github.com/BeeBeansTechnologies/SiTCP_Netlist_for_Kintex7) git submodule:
```bash
    - git submodule sync --recursive
    - git submodule update --init --recursive
```

Make bitfile:
```bash
    - mkdir firmware/vivado/work
    - cd firmware/vivado/work
    - vivado -mode tcl -source ../vivado/make.tcl
```

Program and run:
```bash
    - vivado -mode tcl -source ../vivado/prog.tcl
    - EMU=1 py.test -s
```


