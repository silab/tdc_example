
`timescale 1ns / 1ps

`include "i2c/i2c.v"
`include "i2c/i2c_core.v"
`include "utils/clock_divider.v"
`include "utils/cdc_pulse_sync.v"


`include "tdc_rx/tdc_rx.v"
`include "tdc_rx/tdc_rx_core.v"
`include "tdc_rx/decode_8b10b.v"
`include "tdc_rx/receiver_logic.v"
`include "tdc_rx/rec_sync.v"

`include "utils/cdc_syncfifo.v"
`include "utils/flag_domain_crossing.v"
`include "utils/3_stage_synchronizer.v"

`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"

`include "seq_gen/seq_gen.v"
`include "seq_gen/seq_gen_core.v"
`include "utils/ramb_8_to_n.v"

module tdc_fw_core (
        input wire BUS_CLK,

        input wire          BUS_RST,
        input wire  [31:0]  BUS_ADD,
        inout wire  [7:0]   BUS_DATA,
        input wire          BUS_RD,
        input wire          BUS_WR,

        input wire           FIFO_READY,
        output wire          FIFO_EMPTY,
        output wire [31:0]   FIFO_DATA,

        input wire I2C_CLK,
        input wire RX_CLK,
        input wire RX_DATA_CLK,

        input wire I2C_SCL, I2C_SDA,
        output wire TS_RESET,
        output wire SIGNAL,
        input wire RX_DATA

    );

    /* -------  MODULE ADREESSES  ------- */
    localparam I2C_BASEADDR = 32'h3000;
    localparam I2C_HIGHADDR = 32'h4000 - 1;

    localparam RX_BASEADDR = 32'h4000;
    localparam RX_HIGHADDR = 32'h5000-1;

    localparam PULSE_BASEADDR = 32'h5000;
    localparam PULSE_HIGHADDR = 32'h6000-1;

    localparam SEQ_GEN_BASEADDR = 32'h1000_0000;
    localparam SEQ_GEN_HIGHADDR = 32'h2000_0000-1;

    // ------- MODULES   ------- //

    i2c #(
        .BASEADDR(I2C_BASEADDR),
        .HIGHADDR(I2C_HIGHADDR),
        .ABUSWIDTH(32),
        .MEM_BYTES(32)
    )  i2c (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .I2C_CLK(I2C_CLK),
        .I2C_SDA(I2C_SDA),
        .I2C_SCL(I2C_SCL)
    );

    tdc_rx #(
        .BASEADDR(RX_BASEADDR),
        .HIGHADDR(RX_HIGHADDR),
        .ABUSWIDTH(32)
    ) tdc_rx (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .RX_CLK(RX_CLK),
        .DATA_CLK(RX_DATA_CLK),

        .RX_DATA(RX_DATA),

        .RX_READY(),
        .RX_8B10B_DECODER_ERR(),
        .RX_FIFO_OVERFLOW_ERR(),

        .FIFO_CLK(BUS_CLK),
        .FIFO_READ(FIFO_READY),
        .FIFO_EMPTY(FIFO_EMPTY),
        .FIFO_DATA(FIFO_DATA),

        .RX_FIFO_FULL(),
        .RX_ENABLED()
    );
    
    pulse_gen #(
        .BASEADDR(PULSE_BASEADDR),
        .HIGHADDR(PULSE_HIGHADDR),
        .ABUSWIDTH(32)
    ) rst_pulse_gen (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .PULSE_CLK(BUS_CLK),
        .EXT_START(1'b0),
        .PULSE(TS_RESET)
    );

    seq_gen #(
        .BASEADDR(SEQ_GEN_BASEADDR),
        .HIGHADDR(SEQ_GEN_HIGHADDR),
        .ABUSWIDTH(32),
        .MEM_BYTES(1024),
        .OUT_BITS(1)
    ) signal_seq_gen (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .SEQ_EXT_START(TS_RESET),
        .SEQ_CLK(BUS_CLK),
        .SEQ_OUT(SIGNAL)
    );

endmodule
