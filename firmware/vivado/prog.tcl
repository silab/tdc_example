
# -----------------------------------------------------------
# Copyright (c) SILAB , Physics Institute, University of Bonn
# -----------------------------------------------------------
#
#   This script creates Vivado projects and bitfiles for the supported hardware platforms
#
#   Start vivado in tcl mode by typing:
#       vivado -mode tcl -source ../vivado/prog.tcl
#

open_hw
connect_hw_server 
current_hw_target 
open_hw_target
current_hw_device [lindex [get_hw_devices] 0]
set devPart [get_property PART [current_hw_device]]
set_property PROGRAM.FILE ./output/BDAQ53.bit [current_hw_device]
program_hw_devices [current_hw_device]


exit
