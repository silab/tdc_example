
`timescale 1ps / 1ps

`include "bram_fifo/bram_fifo.v"
`include "bram_fifo/bram_fifo_core.v"
`include "utils/fifo_32_to_8.v"
`include "utils/generic_fifo.v"
`include "utils/bus_to_ip.v"

`include "firmware/src/tdc_fw_core.sv"
`include "dut/src/tdc_top.sv"

module tb (
    input wire          BUS_CLK,
    input wire          BUS_RST,
    input wire  [31:0]  BUS_ADD,
    inout wire  [31:0]  BUS_DATA,
    input wire          BUS_RD,
    input wire          BUS_WR,
    output wire         BUS_BYTE_ACCESS
);

localparam FIFO_BASEADDR = 32'h8000;
localparam FIFO_HIGHADDR = 32'h9000-1;

localparam FIFO_BASEADDR_DATA = 32'h8000_0000;
localparam FIFO_HIGHADDR_DATA = 32'h9000_0000;

localparam RESET_DELAY = 5e3;

reg RESET_N;
initial begin
    RESET_N = 1'b0;
    #(RESET_DELAY) RESET_N = 1'b1;
end

assign BUS_BYTE_ACCESS = (BUS_ADD > 32'd0 && BUS_ADD < 32'h8000_0000) ? 1'b1 : 1'b0;


// -------  USER MODULES  ------- //
wire FIFO_FULL, FIFO_EMPTY;
wire [31:0] FIFO_DATA;

bram_fifo #(
    .BASEADDR(FIFO_BASEADDR),
    .HIGHADDR(FIFO_HIGHADDR),
    .BASEADDR_DATA(FIFO_BASEADDR_DATA),
    .HIGHADDR_DATA(FIFO_HIGHADDR_DATA),
    .ABUSWIDTH(32)
) bram_fifo (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READ_NEXT_OUT(),
    .FIFO_EMPTY_IN(FIFO_EMPTY),
    .FIFO_DATA(FIFO_DATA),

    .FIFO_NOT_EMPTY(),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(),
    .FIFO_READ_ERROR()
);

wire I2C_CLK;
clock_divider #(.DIVISOR(32)) clock_divisor_i2c (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(I2C_CLK)
);

wire RX_DATA_CLK;    
clock_divider #(.DIVISOR(10)) clock_divisor_rx_data (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(RX_DATA_CLK)
);

wire I2C_SCL, I2C_SDA, TS_RESET, SIGNAL, RX_DATA;
tdc_fw_core fw_core(
    // clocks from PLL
    .BUS_CLK(BUS_CLK),
    .I2C_CLK(BUS_CLK),
    .RX_CLK(BUS_CLK),
    .RX_DATA_CLK(RX_DATA_CLK),

    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READY(!FIFO_FULL),
    .FIFO_DATA(FIFO_DATA),
    .FIFO_EMPTY(FIFO_EMPTY),

    .I2C_SCL(I2C_SCL), 
    .I2C_SDA(I2C_SDA),
    .TS_RESET(TS_RESET),
    .SIGNAL(SIGNAL),
    .RX_DATA(RX_DATA)
);

    pullup  isda (I2C_SDA);
    pullup  iscl (I2C_SCL);
    wire SDA_OE, SDA_OUT;
    assign I2C_SDA = SDA_OE ? SDA_OUT : 1'bz;

    tdc_top tdc_top (
        .CLK(BUS_CLK),
        .RESETB(!BUS_RST),
        .TS_RESET(TS_RESET),
        .SIGNAL(SIGNAL),

        .SDA_IN(I2C_SDA), 
        .SDA_OUT(SDA_OUT), 
        .SDA_OE(SDA_OE), 
        .SCL(I2C_SCL),
        .DATA_OUT(RX_DATA)
    );

`ifdef WAVES
initial begin
    $dumpfile("/tmp/tdc.vcd");
    $dumpvars(0);
end
`endif

endmodule
