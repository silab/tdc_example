
module i2c_core_conf (
    input wire RESETB,
    input wire SCL,
    input wire SDA_IN,
    output reg SDA_OUT,
    output reg SDA_OE,

    output reg [7:0] OUT,
    input wire [7:0] IN
);


// TODO: check initial

parameter ADDRESS = 7'b1001001;

reg START;

always @(negedge SDA_IN or negedge SCL)
    if(~SCL)
        START <= 0;
    else
        START <= 1;

reg [7:0] REC_ADDRESS;

localparam STATE_IDLE  = 0, STATE_START = 1, STATE_ADDR = 2, STATE_AACK = 4, STATE_DATA_W = 5, STATE_DATA_R = 6, STATE_DACK_W = 7, STATE_DACK_R = 8, STATE_DACK_LAST = 9, STATE_STOP = 10;

reg [3:0] state, next_state;

reg [2:0] bit_count;
reg [15:0] byte_count;

always @(posedge SCL or posedge START) begin
    if (START)
        state <= STATE_ADDR;
    else
        state <= next_state;
end

//rec stop
always @(*) begin
    next_state = state;
    case(state)
        STATE_IDLE:
            next_state = state;
        STATE_ADDR:
            if(bit_count==7)
                next_state = STATE_AACK;
        STATE_AACK:
            if(REC_ADDRESS[7:1] == ADDRESS) begin
                if(REC_ADDRESS[0])
                    next_state = STATE_DATA_R;
                else
                    next_state = STATE_DATA_W;
            end
            else
                next_state = STATE_IDLE;
        STATE_DATA_R:
            if(bit_count==7)
                next_state = STATE_DACK_R;
        STATE_DATA_W:
            if(bit_count==7)
                next_state = STATE_DACK_W;
        STATE_DACK_W:
            next_state = STATE_DATA_W;
        STATE_DACK_R:
            if(SDA_IN==0)
                next_state = STATE_DATA_R;
            else
                next_state = STATE_IDLE;
    endcase
end

always @(posedge SCL or posedge START) begin
    if(START)
        bit_count <= 0;
    else if (state == STATE_AACK | state == STATE_DACK_R | state == STATE_DACK_W )
        bit_count <= 0;
    else
        bit_count <= bit_count + 1;
end

always @(posedge SCL or posedge START) begin
    if (START)
        byte_count <= 0;
    else if(next_state == STATE_DACK_W | next_state == STATE_DACK_R)
        byte_count <= byte_count + 1;
end

always @(posedge SCL)
    if(state == STATE_ADDR)
        REC_ADDRESS[7-bit_count] = SDA_IN;

reg [7:0] BYTE_DATA_IN;
always @(posedge SCL)
    if(state == STATE_DATA_W)
        BYTE_DATA_IN[7-bit_count] = SDA_IN;

always @(posedge SCL or negedge RESETB) begin
    if(~RESETB)
        OUT <= 8'd0;
    else if(state == STATE_DACK_W)
        OUT <= BYTE_DATA_IN;
end

wire [7:0] BYTE_DATA_OUT;
assign BYTE_DATA_OUT = IN;

wire SDA_OE_PRE;
assign SDA_OE_PRE = ((state == STATE_AACK & REC_ADDRESS[7:1] == ADDRESS) | state == STATE_DACK_W) | state == STATE_DATA_R;

wire SDA_PRE;
assign SDA_PRE = ((state == STATE_AACK & REC_ADDRESS[7:1] == ADDRESS) | state == STATE_DACK_W) ? 1'b0 : state == STATE_DATA_R ? BYTE_DATA_OUT[7-bit_count] : 1;

always@(negedge SCL)
    SDA_OE <= SDA_OE_PRE;

always @(negedge SCL)
    SDA_OUT <= SDA_PRE;

endmodule
